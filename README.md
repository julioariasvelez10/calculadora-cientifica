using System;

public class Calculadora
{
    public double Sumar(double num1, double num2)
    {
        return num1 + num2;
    }

    public double Restar(double num1, double num2)
    {
        return num1 - num2;
    }

    public double Multiplicar(double num1, double num2)
    {
        return num1 * num2;
    }

    public double Dividir(double num1, double num2)
    {
        if (num2 != 0)
        {
            return num1 / num2;
        }
        else
        {
            throw new DivideByZeroException("Error: división por cero");
        }
    }
}
